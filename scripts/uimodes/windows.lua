local async = require('openmw.async')
local ui = require('openmw.ui')
local util = require('openmw.util')
local I = require('openmw.interfaces')

local MODE = I.UI.MODE
local WINDOW = I.UI.WINDOW
local v2 = util.vector2

local M = {}

M.mode = nil
M.selectedOption = nil
modeMenu = nil

local function isWindowAllowed(window)
    return I.UI.getWindowsForMode(MODE.Interface)[window]
end

function M.toggleJournal()
    if I.UI.getMode() == MODE.Journal then
        I.UI.removeMode(MODE.Journal)
        M.mode = nil
    elseif isWindowAllowed(WINDOW.Magic) then
        I.UI.removeMode(MODE.Interface)
        I.UI.addMode(MODE.Journal)
        M.mode = 'journal'
    end
end

function M.toggleMap()
    if I.UI.getMode() == MODE.Interface and mode == 'map' then
        I.UI.removeMode(MODE.Interface)
        M.mode = nil
    elseif isWindowAllowed(WINDOW.Map) then
        I.UI.setMode(MODE.Interface, {windows = {WINDOW.Map}})
        M.mode = 'map'
        M.closeModeMenu()
    end
end

function M.toggleInventoryAndStats()
    if I.UI.getMode() == MODE.Interface and mode == 'stats' then
        I.UI.removeMode(MODE.Interface)
        M.mode = nil
    elseif isWindowAllowed(WINDOW.Inventory) or isWindowAllowed(WINDOW.Stats) then
        I.UI.setMode(MODE.Interface, {windows = {WINDOW.Inventory, WINDOW.Stats}})
        M.mode = 'stats'
        M.closeModeMenu()
    end
end

function M.toggleInventoryAndMagic()
    if I.UI.getMode() == MODE.Interface and mode == 'magic' then
        I.UI.removeMode(MODE.Interface)
        M.mode = nil
    elseif isWindowAllowed(WINDOW.Inventory) or isWindowAllowed(WINDOW.Magic) then
        I.UI.setMode(MODE.Interface, {windows = {WINDOW.Inventory, WINDOW.Magic}})
        M.mode = 'magic'
        M.closeModeMenu()
    end
end

local selectedColor = util.color.rgb(1, 0.3, 0)

local function createModeOption(pos, text, fn)
    return {
        type = ui.TYPE.Text,
        template = I.MWUI.templates.textNormal,
        props = {
            text = text,
            multiline = true,
            anchor = pos,
            relativePosition = pos,
        },
        events = {
            mouseClick = async:callback(fn),
        },
    }
end

local menuOptions = {
    createModeOption(v2(0.5, 0.1), 'Journal', M.toggleJournal),
    createModeOption(v2(0.5, 0.9), 'Map', M.toggleMap),
    createModeOption(v2(0.1, 0.5), 'Inventory\n & Spells', M.toggleInventoryAndMagic),
    createModeOption(v2(0.9, 0.5), 'Inventory\n & Stats', M.toggleInventoryAndStats),
}

local modeMenuLayout = {
  layer = 'Windows',
  type = ui.TYPE.Image,
  props = {
    size = v2(300, 300),
    relativePosition = v2(0.5, 0.5),
    anchor = v2(0.5, 0.5),
    resource = ui.texture{path = 'textures/UiModes/modeMenu.png'},
  },
  content = ui.content(menuOptions),
}

function M.setSelectedOption(fn)
    menuOptions[1].props.textColor = fn == M.toggleJournal and selectedColor or nil
    menuOptions[2].props.textColor = fn == M.toggleMap and selectedColor or nil
    menuOptions[3].props.textColor = fn == M.toggleInventoryAndMagic and selectedColor or nil
    menuOptions[4].props.textColor = fn == M.toggleInventoryAndStats and selectedColor or nil
    M.selectedOption = fn
    modeMenu:update()
end

function M.isModeMenuOpened()
    return modeMenu ~= nil
end

function M.openModeMenu()
    modeMenu = modeMenu or ui.create(modeMenuLayout)
    M.setSelectedOption(nil)
    I.UI.setMode(MODE.Interface, {windows = {}})
end

function M.closeModeMenu()
    if modeMenu then
        modeMenu:destroy()
        modeMenu = nil
        M.selectedOption = nil
    end
end

return M

